class GameChannel < ApplicationCable::Channel
  def subscribed
    stream_from "player_#{uuid}"
    Seek.create(uuid)
  end

  def unsubscribed
    Seek.remove(uuid)
    Game.forfeit(uuid)
  end

  def move(data)
    puts data
    Game.move(uuid,data)
  end

  def take_turn(data)
    Game.take_turn(data['data'])
  end

end
