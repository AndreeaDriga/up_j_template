App.game = App.cable.subscriptions.create "GameChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    switch data.action

      when "game_start"

        $('.enemy_map').css
          'background-color' : 'transparent'
        @printTeamName("#{data.team}")
        if data.turn then @printMessage("Game started! It's your turn.") else @printMessage("Game started! It's your opponent turn.") 
        if !data.turn then $('.enemy_map').css 'pointerEvents', 'none'
        build_my_map data.board
        


      when "opponent_forfeits"

        @printMessage("Opponent forfeits. You win!")
        $('.enemy_map').css 'pointerEvents', 'none'


      when "update_my_game"

        id = data.grid.data
        if data.res == 7
          @printMessage("You missed! It's your opponent turn.")
          $('.enemy_map').css 'pointerEvents', 'none'
          $("#" + id).text('0')
          $("#" + id).css
            'background-color': 'red'
            'font-size' : '16px'
            'color' : 'black'
        else if data.res == 1
          $('.enemy_map').css 'pointerEvents', 'none'
          $("#" + id).text('X')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'white'
            'background-color' : 'black'
          team = $("#teamName").text()
          if team == 'ruby'
              @printMessage("OCS Lifeboat was sunk! Good strike! The win is comming. It's your opponents turn.")
              @printSunkedShip("OCS Lifeboat was sunk!")
          else if team == 'java'
              @printMessage("BB Diamond Lifeboat was sunk! Good strike! The win is comming. It's your opponents turn.")
              @printSunkedShip("BB Diamond Lifeboat was sunk!")

        else if data.res == 100
          @printMessage("You missed! It's still your turn.")
          $('.enemy_map').css 'pointerEvents', 'auto'
          $("#" + id).text('0')
          $("#" + id).css
            'background-color': 'red'
            'font-size' : '16px'
            'color' : 'black'

        else if data.res == 1000
          @printMessage("Good strike! It's still your turn.")
          $("#" + id).text('X')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'white'
            'background-color' : 'black'
          team = $("#teamName").text()
          if data.destroy != 0
            if data.destroy == 2 and  team == 'ruby'
              @printMessage("P2P Destroyer was sunk! It's still your turn.")
              @printSunkedShip("P2P Destroyer was sunk!")
            else if data.destroy == 2 and team == 'java'
              @printMessage("Coala Destroyer was sunk! It's still your turn.")
              @printSunkedShip("Coala Destroyer was sunk!")
            if data.destroy == 3 and  team == 'ruby'
              @printMessage("Fish Battlecruiser was sunk! It's still your turn.")
              @printSunkedShip("Fish Battlecruiser was sunk!")
            else if data.destroy == 3 and team == 'java'
              @printMessage("Federgon Battlecruiser was sunk! It's still your turn.")
              @printSunkedShip("Federgon Battlecruiser was sunk!")
            if data.destroy == 4 and  team == 'ruby'
              @printMessage("Eagle Battleship was sunk! It's still your turn.")
              @printSunkedShip("Eagle Battleship was sunk!")
            else if data.destroy == 4 and team == 'java'
              @printMessage("Cobra Battleship was sunk! It's still your turn.")
              @printSunkedShip("Cobra Battleship was sunk!")
            if data.destroy == 5 and  team == 'ruby'
              @printMessage("MOLE Carrier was sunk! It's still your turn.")
              @printSunkedShip("MOLE Carrier was sunk!")
            else if data.destroy == 5 and team == 'java'
              @printMessage("Community Mothership was sunk! It's still your turn.")
              @printSunkedShip("Community Mothership was sunk!")
            if data.destroy == 10 and  team == 'ruby'
              @printMessage("F'ox Submarine was sunk! It's still your turn.")
              @printSunkedShip("F'ox Submarine was sunk!")
            else if data.destroy == 10 and team == 'java'
              @printMessage("Bill2Box Submarine was sunk! It's still your turn.")
              @printSunkedShip("Bill2Box Submarine was sunk!")


        else
          @printMessage("Good strike! It's still your turn.")
          $("#" + id).text('X')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'white'
            'background-color' : 'black'
          team = $("#teamName").text()
          if data.destroy != data.res
            if data.res == 2 and  team == 'ruby'
              @printMessage("P2P Destroyer was sunk! It's still your turn.")
              @printSunkedShip("P2P Destroyer was sunk!")
            else if data.res == 2 and team == 'java'
              @printMessage("Coala Destroyer was sunk! It's still your turn.")
              @printSunkedShip("Coala Destroyer was sunk!")
            if data.res == 3 and  team == 'ruby'
              @printMessage("Fish Battlecruiser was sunk! It's still your turn.")
              @printSunkedShip("Fish Battlecruiser was sunk!")
            else if data.res == 3 and team == 'java'
              @printMessage("Federgon Battlecruiser was sunk! It's still your turn.")
              @printSunkedShip("Federgon Battlecruiser was sunk!")
            if data.res == 4 and  team == 'ruby'
              @printMessage("Eagle Battleship was sunk! It's still your turn.")
              @printSunkedShip("Eagle Battleship was sunk!")
            else if data.res == 4 and team == 'java'
              @printMessage("Cobra Battleship was sunk! It's still your turn.")
              @printSunkedShip("Cobra Battleship was sunk!")
            if data.res == 5 and  team == 'ruby'
              @printMessage("MOLE Carrier was sunk! It's still your turn.")
              @printSunkedShip("MOLE Carrier was sunk!")
            else if data.res == 5 and team == 'java'
              @printMessage("Community Mothership was sunk! It's still your turn.")
              @printSunkedShip("Community Mothership was sunk!")
            if data.res == 10 and  team == 'ruby'
              @printMessage("F'ox Submarine was sunk! It's still your turn.")
              @printSunkedShip("F'ox Submarine was sunk!")
            else if data.res == 10 and team == 'java'
              @printMessage("Bill2Box Submarine was sunk! It's still your turn.")
              @printSunkedShip("Bill2Box Submarine was sunk!")




      when "update_opponent_game"

        id = (data.grid.data).toLowerCase()
        if data.res == 7
          @printMessage("He missed! It's your turn now.")
          $('.enemy_map').css 'pointerEvents', 'auto'
          $("#" + id).text('0')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'black'
        else if data.res == 1
          @printMessage("There is no more time left. It's your turn.")
          $('.enemy_map').css 'pointerEvents', 'auto'
          $("#" + id).text('X')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'white'
          team = $("#teamName").text()
          if team == 'java'
              @printMessage("OCS Lifeboat was sunk! It's your turn.")
          else if team == 'ruby'
              @printMessage("BB Diamond Lifeboat was sunk! It's your turn.")

        else if data.res == 100
          @printMessage("He missed! It's still your opponent turn.")
          $('.enemy_map').css 'pointerEvents', 'none'
          $("#" + id).text('0')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'black'

        else if data.res == 1000
          @printMessage("It's still his turn.")
          $("#" + id).text('X')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'white'

          team = $("#teamName").text()
          if data.destroy != 0
            if data.destroy == 2 and  team == 'java'
              @printMessage("P2P Destroyer was sunk! It's your opponents turn.")
            else if data.destroy == 2 and team == 'ruby'
              @printMessage("Coala Destroyer was sunk! It's your opponents turn.")
            if data.destroy == 3 and  team == 'java'
              @printMessage("Fish Battlecruiser was sunk! It's your opponents turn.")
            else if data.destroy == 3 and team == 'ruby'
              @printMessage("Federgon Battlecruiser was sunk! It's your opponents turn.")
            if data.destroy == 4 and  team == 'java'
              @printMessage("Eagle Battleship was sunk! It's your opponents turn.")
            else if data.destroy == 4 and team == 'ruby'
              @printMessage("Cobra Battleship was sunk! It's your opponents turn.")
            if data.destroy == 5 and  team == 'java'
              @printMessage("MOLE Carrier was sunk! It's your opponents turn.")
            else if data.destroy == 5 and team == 'ruby'
              @printMessage("Community Mothership was sunk! It's your opponents turn.")
            if data.destroy == 10 and  team == 'java'
              @printMessage("F'ox Submarine was sunk! It's your opponents turn.")
            else if data.destroy == 10 and team == 'ruby'
              @printMessage("Bill2Box Submarine was sunk! It's your opponents turn.")

        else
          @printMessage("He had a good hit. It's still his turn.")
          $("#" + id).text('X')
          $("#" + id).css
            'font-size' : '16px'
            'color' : 'white'

          team = $("#teamName").text()
          if data.destroy != data.res
            if data.res == 2 and  team == 'java'
              @printMessage("P2P Destroyer was sunk! It's your opponents turn.")
            else if data.res == 2 and team == 'ruby'
              @printMessage("Coala Destroyer was sunk! It's your opponents turn.")
            if data.res == 3 and  team == 'java'
              @printMessage("Fish Battlecruiser was sunk! It's your opponents turn.")
            else if data.res == 3 and team == 'ruby'
              @printMessage("Federgon Battlecruiser was sunk! It's your opponents turn.")
            if data.res == 4 and  team == 'java'
              @printMessage("Eagle Battleship was sunk! It's your opponents turn.")
            else if data.res == 4 and team == 'ruby'
              @printMessage("Cobra Battleship was sunk! It's your opponents turn.")
            if data.res == 5 and  team == 'java'
              @printMessage("MOLE Carrier was sunk! It's your opponents turn.")
            else if data.res == 5 and team == 'ruby'
              @printMessage("Community Mothership was sunk! It's your opponents turn.")
            if data.res == 10 and  team == 'java'
              @printMessage("F'ox Submarine was sunk! It's your opponents turn.")
            else if data.res == 10 and team == 'ruby'
              @printMessage("Bill2Box Submarine was sunk! It's your opponents turn.")

      when "win"
        @printMessage("Well done. Victory belongs to the most persevering.")
        alert("Well done. Victory belongs to the most persevering.")
        $('.enemy_map').css 'pointerEvents', 'none'

      when "lose"
        @printMessage("Sometimes by losing a battle you find a new way to win the war. Try again.")
        alert("Sometimes by losing a battle you find a new way to win the war. Try again.")
        $('.enemy_map').css 'pointerEvents', 'none'


    return





  move: (id)->
    @perform("move", data: id)

  printMessage: (message) ->
    $("#messages").html("<div>#{message}</div>")

  printTeamName: (message) ->
    $("#teamName").append("<button class='paragraph'>#{message}</button>")

  printSunkedShip: (message) ->
    $("#sunked").append("<div>#{message}</div>")


  build_my_map = (board)->
  
    i = 0
    j = 0
    k = 0
    x = []
    while i < 12
      j = 0
      while j < 12
        x[k] = board[i][j]
        k++
        j++
      i++
    
    i = 0
    $('.my_map').each ->
      $(this).text x[i]
      if x[i] != 7
        $(this).css
          'background-color': 'black'
          'color': 'red'
          'font-size': '0px'
      i++
      return
    return

  
$(document).ready -> 
  $('.enemy_map').click ->
    $(this).addClass 'enemy_map_struck'
    id = $(this).attr('id')
    App.game.move(id)
    $('#displayedValue').html 'Value: ' + id
  return

