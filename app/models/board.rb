class Board < ApplicationRecord
  belongs_to :player

  # after_create :initialize_state
  # after_create :build_battle_grid

  # http://api.rubyonrails.org/classes/ActiveRecord/AttributeMethods/Serialization/ClassMethods.html
  serialize :grid

  MATRIX_SIZE = 12

  FLEET = [5, 4, 3, 3, 2, 1]
  HORIZONTAL = 'O'
  VERTICAL = 'V'
  DIRECTION = [HORIZONTAL, VERTICAL]

  WATER = 7
  #SHIP = 1
  #MISS = 2
  #HIT = 3

  @@first_3ship = 0

  def initialize
    super
    initialize_state
  end

  def initialize_state
    self.grid = []
    (0...MATRIX_SIZE).each do |row|
      self.grid[row] = [WATER] * MATRIX_SIZE
    end
    self
  end

  def build_battle_grid
    Board::FLEET.shuffle.each do |ship_length|
      space = available_placements(ship_length).sample
      place_ship(space[0], space[1], ship_length, space[2], ship_length)
    end
    self
  end

  # Places ship on battle grid
  def place_ship(x, y, length, direction, ship)
    ship_coordinates = get_coordinates(x, y, length, direction)
    ship_coordinates.each do |space|
      if ship == 3
        if @@first_3ship < 3
          self.grid[space[0]][space[1]] = ship
          @@first_3ship += 1
        else
          self.grid[space[0]][space[1]] = 10
          @@first_3ship += 1
          if @@first_3ship == 6
            @@first_3ship = 0
          end
        end      
      else
        self.grid[space[0]][space[1]] = ship
      end
    end
  end

  def available_placements(length)
    available_placements = []
    self.grid.each_with_index do |x, xi|
      x.each_with_index do |y, yi|
        DIRECTION.each do |direction|
          if valid_placement?(xi, yi, length, direction)
            available_placements << [xi, yi, direction]
          end
        end
      end
    end

    available_placements
  end

  # Returns true if placement on grid and available
  def valid_placement?(x, y, length, direction)
    coordinates = get_coordinates(x, y, length, direction)
    return false if !coordinates

    coordinates.each do |coordinate|
      return false if self.grid[coordinate[0]][coordinate[1]] != WATER
    end
    true
  end

  def get_coordinates(x, y, length, direction)
    coordinates = []
    end_coordinate = calculate_end_coordinate(x, y, length, direction)

    return false if is_outside_of_grid?(end_coordinate[0], end_coordinate[1])

    (0...length).each do |size|
      case direction
        when HORIZONTAL
          coordinates << [x + size, y]
        when VERTICAL
          coordinates << [x, y + size]
      end
    end

    coordinates
  end


  def calculate_end_coordinate(start_x, start_y, length, direction)
    case direction
      when VERTICAL
        return [start_x, start_y + length]
      when HORIZONTAL
        return [start_x + length, start_y]
      else
        raise ArgumentError, "Direction '#{direction}' is invalid. Must be '#{VERTICAL}' or '#{HORIZONTAL}'"
    end
  end

  # Returns false if coordinate outside of grid
  def is_outside_of_grid?(x, y)
    return true if (x < 0 || x > Board::MATRIX_SIZE - 1)
    return true if (y < 0 || y > Board::MATRIX_SIZE - 1)
    false
  end
end
