class Game < ApplicationRecord
  has_many :players

  	def self.start(uuid1, uuid2)
		java = uuid1
		ruby = uuid2
		first_player = [java, ruby].sample
		flag = 0

		java_board = Board.new.build_battle_grid.grid
    	ruby_board = Board.new.build_battle_grid.grid

		ActionCable.server.broadcast "player_#{java}", {action: "game_start" , enemy_uuid: ruby, board: java_board, team: "java", turn: first_player == java ? true : false}
		ActionCable.server.broadcast "player_#{ruby}", {action: "game_start" , enemy_uuid: java, board: ruby_board, team: "ruby", turn: first_player == java ? false : true}

		REDIS.set(uuid1, Marshal.dump(java_board))
		REDIS.set(uuid2, Marshal.dump(ruby_board))
		REDIS.set("flag_for:#{java}", flag)
		REDIS.set("flag_for:#{ruby}", flag)

		REDIS.set("opponent_for:#{java}", ruby)
		REDIS.set("opponent_for:#{ruby}", java)
	end

	def self.forfeit(uuid)
    	if winner = opponent_for(uuid)
     		ActionCable.server.broadcast "player_#{winner}", {action: "opponent_forfeits"}
    	end
  	end

	def self.opponent_for(uuid)
		REDIS.get("opponent_for:#{uuid}")
	end

	def self.move(uuid, data)
		opponent_uuid = opponent_for(uuid)
		flag = (REDIS.get("flag_for:#{uuid}")).to_i

		shot_result = 0
		shot_coordinates = data["data"].split('-')
		col = shot_coordinates[0].ord - 65
		row = shot_coordinates[1].to_i - 1
		opponent_map = Marshal.load(REDIS.get(opponent_uuid))
		shot_result = opponent_map[row][col]

		if (shot_result != "0" && shot_result != "X")
			if (flag == 0)
				ship = 0
				if (shot_result == 7)
					opponent_map[row][col] = "0"
					ship = shot_result
					REDIS.set(opponent_uuid, Marshal.dump(opponent_map))

					ActionCable.server.broadcast "player_#{uuid}", {action: "update_my_game", grid: data, res: shot_result, destroy: ship}
					ActionCable.server.broadcast "player_#{opponent_uuid}", {action: "update_opponent_game", grid: data, res: shot_result, destroy: ship}
				
				elsif (shot_result == 1)
					opponent_map[row][col] = "X"
					gasit = false
					opponent_map.each do |sub|
						sub.each do |element|
							if (element != "X" && element != "0" && element != 7 && gasit == false)
								gasit = true
							end
						end
					end

					if gasit == false
						ActionCable.server.broadcast "player_#{uuid}", {action: "win"}
						ActionCable.server.broadcast "player_#{opponent_uuid}", {action: "lose"}
					end

					flag += 1
					REDIS.set(opponent_uuid, Marshal.dump(opponent_map))
					REDIS.set("flag_for:#{opponent_uuid}", flag)

					ActionCable.server.broadcast "player_#{uuid}", {action: "update_my_game", grid: data, res: shot_result, destroy: ship}
					ActionCable.server.broadcast "player_#{opponent_uuid}", {action: "update_opponent_game", grid: data, res: shot_result, destroy: ship}

					
				else
					opponent_map[row][col] = "X"

					gasit = false
					opponent_map.each do |sub|
						sub.each do |element|
							if element == shot_result && gasit == false
								ship = shot_result
								gasit = true
							end
						end
					end

					REDIS.set(opponent_uuid, Marshal.dump(opponent_map))

					ActionCable.server.broadcast "player_#{uuid}", {action: "update_my_game", grid: data, res: shot_result, destroy: ship}
					ActionCable.server.broadcast "player_#{opponent_uuid}", {action: "update_opponent_game", grid: data, res: shot_result, destroy: ship}
				end

			elsif (flag < 4)
				ship = 0
				if (shot_result == 7)
					opponent_map[row][col] = "0"
					flag += 1
					REDIS.set("flag_for:#{uuid}", flag)
					REDIS.set(opponent_uuid, Marshal.dump(opponent_map))

					shot_result = 100

					ActionCable.server.broadcast "player_#{uuid}", {action: "update_my_game", grid: data, res: shot_result, destroy: ship}
					ActionCable.server.broadcast "player_#{opponent_uuid}", {action: "update_opponent_game", grid: data, res: shot_result, destroy: ship}
					
				else
					ship = shot_result
					opponent_map[row][col] = "X"
					gasit = false
					opponent_map.each do |sub|
						sub.each do |element|
							if element == shot_result && gasit == false
								ship = 0
								gasit = true
							end
						end
					end

					shot_result = 1000

					REDIS.set(opponent_uuid, Marshal.dump(opponent_map))

					ActionCable.server.broadcast "player_#{uuid}", {action: "update_my_game", grid: data, res: shot_result, destroy: ship}
					ActionCable.server.broadcast "player_#{opponent_uuid}", {action: "update_opponent_game", grid: data, res: shot_result, destroy: ship}
				end


			elsif (flag == 4)
				ActionCable.server.broadcast "player_#{uuid}", {action: "lose"}
				ActionCable.server.broadcast "player_#{opponent_uuid}", {action: "win"}
					
			end		
		end
	end

end
