class BoardController < ApplicationController
  def index
    @board1 = Board.new
    @board2 = Board.new
  end

  def homePage
  	render 'homePage'
  end


  def generate
    redirect_to board_index_path
  end
end
