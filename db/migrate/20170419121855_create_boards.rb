class CreateBoards < ActiveRecord::Migration[5.0]
  def change
    create_table :boards do |t|
      t.text :grid
      t.references :player, foreign_key: true

      t.timestamps null:false
    end
  end
end
