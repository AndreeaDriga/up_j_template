Rails.application.routes.draw do
  get 'token/token_input'

  get 'board/index'
  get 'board/generate'
  get 'board/homePage'

  get 'player/index'

  root 'board#homePage'

  mount ActionCable.server => "/cable"
  #root 'board#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
